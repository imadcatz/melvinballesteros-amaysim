//
//  OverlayView.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "OverlayView.h"

@implementation OverlayView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setAlpha:0];
        [self initAnimationDisplay];
        [self initializedBackground];
        [self initActivityIndicator];
    }
    return self;
}

-(void)initAnimationDisplay {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [self setAlpha:1];
    [UIView commitAnimations];
}

-(void)initializedBackground {
    [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
}

-(void)initActivityIndicator {
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((screenWidth-50)/2, (screenHeight-50)/2, 50, 50)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.color = UIColorFromRGB(0xE65013);
    [self addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
}

-(void)removeOverlay {
    [UIView animateWithDuration:0.4 animations:^{
        [self setAlpha:0];
    }completion:^(BOOL finished) {
        [activityIndicator stopAnimating];
        [self removeFromSuperview];
    }];
}

@end
