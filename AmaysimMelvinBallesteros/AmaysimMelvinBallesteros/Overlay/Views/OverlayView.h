//
//  OverlayView.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverlayView : UIView {
    UIActivityIndicatorView *activityIndicator;
}

-(id)initWithFrame:(CGRect)frame;
-(void)removeOverlay;

@end
