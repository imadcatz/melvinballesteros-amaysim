//
//  AccountService.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountModel.h"
#import "Helper+Json.h"

@interface AccountService : NSObject

+(AccountModel *)initCreateAccountModel;

@end
