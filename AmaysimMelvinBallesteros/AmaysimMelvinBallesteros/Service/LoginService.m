//
//  LoginService.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "LoginService.h"

@implementation LoginService

+(void)serviceLoginAnimateLogo:(UIImageView *)imageView {
    [UIView animateWithDuration: 0.5 delay: 0.0 options: UIViewAnimationOptionCurveEaseIn animations:^{
         CGRect frame = imageView.frame;
         frame.origin.y = 100;
         imageView.frame = frame;
    } completion:nil];
}

+(BOOL)serviceloginAuthenticateUsername:(NSString *)username password:(NSString *)password {
    NSDictionary *jsonDictionary = [Helper_Json helperGetJsonFile];
    AccountModel *model = [[AccountModel alloc] initWithDictionary:[Helper_Json helperGetAccounts:jsonDictionary]];
    
    if ([username isEqualToString:model.accountEmail] && [password isEqualToString:model.accountID]) {
        return YES;
    } else {
        return NO;
    }
}

@end
