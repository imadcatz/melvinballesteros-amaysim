//
//  AccountService.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "AccountService.h"

@implementation AccountService

+(AccountModel *)initCreateAccountModel {
    NSDictionary *jsonDictionary = [Helper_Json helperGetJsonFile];
    AccountModel *model = [[AccountModel alloc] initWithDictionary:[Helper_Json helperGetAccounts:jsonDictionary]];
    return model;
}

@end
