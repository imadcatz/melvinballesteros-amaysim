//
//  LoginService.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper+Json.h"
#import "AccountModel.h"

@interface LoginService : NSObject

+(void)serviceLoginAnimateLogo:(UIImageView *)imageView;
+(BOOL)serviceloginAuthenticateUsername:(NSString *)username password:(NSString *)password;

@end
