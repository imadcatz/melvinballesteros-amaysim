//
//  AmaysimApplication.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "AmaysimApplication.h"

static AmaysimApplication *_sharedInstance;

@implementation AmaysimApplication

+(AmaysimApplication *)sharedInstance {
    if (!_sharedInstance)
        _sharedInstance = [[AmaysimApplication alloc]init];
    return _sharedInstance;
}

@end
