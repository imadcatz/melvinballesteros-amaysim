//
//  AmaysimApplication.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OverlayView.h"

@interface AmaysimApplication : NSObject {
    
}

+(AmaysimApplication * )sharedInstance;


@property (nonatomic, strong) OverlayView *overlayView;

@end
