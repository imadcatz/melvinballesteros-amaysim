//
//  ProductView.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//
#import "ProductView.h"

@implementation ProductView

-(id)initWithFrame:(CGRect)frame model:(ProductModel *)model {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        productModel = model;
        [self initCreateLabels];
        [self initCreateBackButton];
    }
    return self;
}

-(void)initCreateLabels {
    lblProductName = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, 30, 200, 30) text:productModel.productName];
    [lblProductName setFrame:CGRectMake((screenWidth-lblProductName.frame.size.width)/2, lblProductName.frame.origin.y, lblProductName.frame.size.width, lblProductName.frame.size.height)];
    [self addSubview:lblProductName];
    
    lblPrice = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblProductName.frame) + 5, 200, 30) text:@"Price"];
    [lblPrice setFrame:CGRectMake((screenWidth-lblPrice.frame.size.width)/2, lblPrice.frame.origin.y, lblPrice.frame.size.width, lblPrice.frame.size.height)];
    [self addSubview:lblPrice];
    
    NSString *getPrice = productModel.productPrice;
    double getDollar = [getPrice doubleValue];
    lblPriceValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblPrice.frame)-2, 200, 30) text:[Helper_Conversion helperConvertCentsToDollar:getDollar]];
    [lblPriceValue setFrame:CGRectMake((screenWidth-lblPriceValue.frame.size.width)/2, lblPriceValue.frame.origin.y, lblPriceValue.frame.size.width, lblPriceValue.frame.size.height)];
    [self addSubview:lblPriceValue];
    
    lblHeaderUnlimitedText = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblPriceValue.frame) + 5, 200, 30) text:@"UNLIMITED TEXT"];
    [lblHeaderUnlimitedText setFrame:CGRectMake((screenWidth-lblHeaderUnlimitedText.frame.size.width)/2, lblHeaderUnlimitedText.frame.origin.y, lblHeaderUnlimitedText.frame.size.width, lblHeaderUnlimitedText.frame.size.height)];
    [self addSubview:lblHeaderUnlimitedText];
    
    lblHeaderUnlimitedTextValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderUnlimitedText.frame)-2, 200, 30) text:productModel.productUnliText];
    [lblHeaderUnlimitedTextValue setFrame:CGRectMake((screenWidth-lblHeaderUnlimitedTextValue.frame.size.width)/2, lblHeaderUnlimitedTextValue.frame.origin.y, lblHeaderUnlimitedTextValue.frame.size.width, lblHeaderUnlimitedTextValue.frame.size.height)];
    [self addSubview:lblHeaderUnlimitedTextValue];
    
    lblHeaderUnlimitedTalk = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderUnlimitedTextValue.frame) + 5, 200, 30) text:@"UNLIMITED TALK"];
    [lblHeaderUnlimitedTalk setFrame:CGRectMake((screenWidth-lblHeaderUnlimitedTalk.frame.size.width)/2, lblHeaderUnlimitedTalk.frame.origin.y, lblHeaderUnlimitedTalk.frame.size.width, lblHeaderUnlimitedTalk.frame.size.height)];
    [self addSubview:lblHeaderUnlimitedTalk];
    
    lblHeaderUnlimitedTalkValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderUnlimitedTalk.frame)-2, 200, 30) text:productModel.productUnliTalk];
    [lblHeaderUnlimitedTalkValue setFrame:CGRectMake((screenWidth-lblHeaderUnlimitedTalkValue.frame.size.width)/2, lblHeaderUnlimitedTalkValue.frame.origin.y, lblHeaderUnlimitedTalkValue.frame.size.width, lblHeaderUnlimitedTalkValue.frame.size.height)];
    [self addSubview:lblHeaderUnlimitedTalkValue];

}

-(void)initCreateBackButton {
    self.btnBack = [[UIButton alloc] initWithFrame:CGRectMake((screenWidth-80)/2, CGRectGetMaxY(lblHeaderUnlimitedTalkValue.frame) + 30, 80, 75)];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
    [self addSubview:self.btnBack];
}


@end
