//
//  ProductView.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
#import "Helper+Labels.h"
#import "Helper+Conversion.h"

@interface ProductView : UIView {
    
    UILabel *lblProductName;
    UILabel *lblPrice;
    UILabel *lblPriceValue;
    UILabel *lblHeaderUnlimitedText;
    UILabel *lblHeaderUnlimitedTextValue;
    UILabel *lblHeaderUnlimitedTalk;
    UILabel *lblHeaderUnlimitedTalkValue;
    
    ProductModel *productModel;

}

-(id)initWithFrame:(CGRect)frame model:(ProductModel *)model;
@property (nonatomic, strong) UIButton *btnBack;

@end
