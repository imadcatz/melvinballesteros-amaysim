//
//  ProductsViewController.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "ProductsViewController.h"

@interface ProductsViewController ()

@end

@implementation ProductsViewController


#pragma mark- View Delegate
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavControllerHide];
    [self initCreateGradientBackground];
    [self initCreateHeaderTitle];
    [self initCreateAppLogo];
    [self initCreateScrollView];
    [self initDataSourceWithView];
}

#pragma mark - Initialization
-(void)initNavControllerHide {
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)initCreateGradientBackground {
    [self.view.layer addSublayer:[Helper_LayerGradient helperGradientLinearBackground:UIColorFromRGB(0xE65013) color2:UIColorFromRGB(0xffffff) view:self.view]];
    [self.view.layer setShouldRasterize:YES];
    [self.view.layer setRasterizationScale:[UIScreen mainScreen].scale];
}

-(void)initCreateHeaderTitle {
    lblHeader = [Helper_Labels helperLabelTitleHeader:CGRectMake((screenWidth-250)/2, 25, 250, 44) text:@"Product"];
    [self.view addSubview:lblHeader];
}


-(void)initCreateAppLogo {
    imgViewAppLogo = [[UIImageView alloc] initWithFrame:CGRectMake((screenWidth-150)/2, screenHeight - 50, 150, 32)];
    [imgViewAppLogo setImage:[UIImage imageNamed:@"AppLogo"]];
    [self.view addSubview:imgViewAppLogo];
}

-(void)initCreateButtonBack {
    UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake((screenWidth-80)/2, screenHeight - 200, 80, 75)];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(actionEventBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];    
}

-(void)initCreateScrollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lblHeader.frame), screenWidth, screenHeight-CGRectGetMaxY(lblHeader.frame))];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setContentSize:CGSizeMake(screenWidth, self.scrollView.frame.size.height + 1)];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    [self.scrollView setDelaysContentTouches:NO];
    [self.view addSubview:self.scrollView];
}

-(void)initDataSourceWithView {
    NSDictionary *jsonDictionary = [Helper_Json helperGetJsonFile];
    NSArray *arrayOfSubscription = [Helper_Json helperGetSubscription:jsonDictionary];                           //-->>Get Data
    ProductModel *model = [[ProductModel alloc] initWithDictionary:arrayOfSubscription];                     //-->>Initialized Model
    
    productView = [[ProductView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 450) model:model];   //-->>Initialized View
    [productView.btnBack addTarget:self action:@selector(actionEventBack) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:productView];                                                           //-->>Add created view in the ScrollView
}

#pragma mark - Action Events
-(void)actionEventBack {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Memory Management
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
