//
//  ProductsViewController.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
#import "Helper+Json.h"
#import "ProductView.h"
#import "Helper+LayerGradient.h"

@interface ProductsViewController : UIViewController {
    UILabel *lblHeader;
    UIImageView *imgViewAppLogo;
    
    ProductView *productView;
}

@property (nonatomic, strong) UIScrollView *scrollView;

@end
