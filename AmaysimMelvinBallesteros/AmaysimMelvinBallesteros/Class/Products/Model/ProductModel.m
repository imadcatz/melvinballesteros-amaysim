//
//  ProductModel.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

NSString *const fieldProductName = @"name";
NSString *const fieldProductPrice = @"price";
NSString *const fieldProductUnlimitedText = @"unlimited-text";
NSString *const fieldProductUnlimitedTalk = @"unlimited-talk";

#import "ProductModel.h"

@implementation ProductModel

-(id)initWithDictionary:(NSArray *)array {
    self.productName = [[[array objectAtIndex:2] objectForKey:@"attributes"] objectForKey:fieldProductName];
    self.productPrice = [[[array objectAtIndex:2] objectForKey:@"attributes"] objectForKey:fieldProductPrice];

    NSString *getUnlitext = [[[array objectAtIndex:2] objectForKey:@"attributes"] objectForKey:fieldProductUnlimitedText];
    NSString *getUnliTalk = [[[array objectAtIndex:2] objectForKey:@"attributes"] objectForKey:fieldProductUnlimitedTalk];
    
    NSInteger convUnliText = [getUnlitext integerValue];
    NSInteger convUnliTalk = [getUnliTalk integerValue];
    
    if (convUnliText == 1) { self.productUnliText = @"YES"; } else { self.productUnliText = @"NO"; }
    if (convUnliTalk == 1) { self.productUnliTalk = @"YES"; } else { self.productUnliTalk = @"NO"; }
    
    return self;
}

@end
