//
//  ProductModel.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

extern NSString *const fieldProductName;
extern NSString *const fieldProductPrice;
extern NSString *const fieldProductUnlimitedText;
extern NSString *const fieldProductUnlimitedTalk;

#import <Foundation/Foundation.h>

@interface ProductModel : NSObject

-(id)initWithDictionary:(NSArray *)array;

@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *productPrice;
@property (nonatomic, strong) NSString *productUnliText;
@property (nonatomic, strong) NSString *productUnliTalk;

@end
