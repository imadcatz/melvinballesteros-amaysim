//
//  SubscriptionModel.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

extern NSString *const fieldSubscriptionAutoRenewal;
extern NSString *const fieldSubscriptionPrimary;
extern NSString *const fieldSubscriptionExpiryDate;
extern NSString *const fieldSubscriptionDataBalance;
extern NSString *const fieldSubscriptionCreditBalance;
extern NSString *const fieldSubscriptionRolloverCreditBalance;
extern NSString *const fieldSubscriptionRolloeverDataBalance;
extern NSString *const fieldSubscriptionInternationalTalkBalance;

#import <Foundation/Foundation.h>

@interface SubscriptionModel : NSObject

-(id)initWithDictionary:(NSArray *)array;

@property (nonatomic, strong) NSString *subAutoRenewal;
@property (nonatomic, strong) NSString *subPrimary;
@property (nonatomic, strong) NSString *subExpiryDate;
@property (nonatomic, strong) NSString *subRollOverCreditBalance;
@property (nonatomic, strong) NSString *subRollOverDataBalance;
@property (nonatomic, strong) NSString *subIncludedCreditBalance;
@property (nonatomic, strong) NSString *subIncludedDataBalance;
@property (nonatomic, strong) NSString *subInternationalTalkBalance;

@end
