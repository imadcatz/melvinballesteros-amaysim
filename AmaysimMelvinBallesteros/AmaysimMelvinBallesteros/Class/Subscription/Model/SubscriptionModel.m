//
//  SubscriptionModel.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

NSString *const fieldSubscriptionAutoRenewal = @"auto-renewal";
NSString *const fieldSubscriptionPrimary = @"primary-subscription";
NSString *const fieldSubscriptionExpiryDate = @"expiry-date";
NSString *const fieldSubscriptionDataBalance = @"included-data-balance";
NSString *const fieldSubscriptionCreditBalance = @"included-credit-balance";
NSString *const fieldSubscriptionRolloverCreditBalance = @"included-rollover-credit-balance";
NSString *const fieldSubscriptionRolloeverDataBalance = @"included-rollover-data-balance";
NSString *const fieldSubscriptionInternationalTalkBalance = @"included-international-talk-balance";

#import "SubscriptionModel.h"

@implementation SubscriptionModel

-(id)initWithDictionary:(NSArray *)array {
    
    NSString *getPrimary = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionPrimary];
    NSString *getRenewal = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionAutoRenewal];
    
    NSInteger convPrimary = [getPrimary integerValue];
    NSInteger convRenewal = [getRenewal integerValue];
    
    if (convPrimary == 1) { self.subPrimary = @"YES"; } else { self.subPrimary = @"NO"; }
    if (convRenewal == 1) { self.subAutoRenewal = @"YES"; } else { self.subAutoRenewal = @"NO"; }

    NSString *getIncludedCreditBalance = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionCreditBalance];
    NSString *getIncludedDataBalance = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionDataBalance];
    NSString *getRollOverDataBalance = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionRolloeverDataBalance];
    NSString *getRollOverCreditBalance = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionRolloverCreditBalance];
    NSString *getInternationalTalkBalance = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionInternationalTalkBalance];
    
    if ([getIncludedCreditBalance isKindOfClass:[NSNull class]]) { self.subIncludedCreditBalance = @"0.0"; } else { self.subIncludedCreditBalance = getIncludedCreditBalance; }
    if ([getIncludedDataBalance isKindOfClass:[NSNull class]]) { self.subIncludedDataBalance = @"0.0"; } else { self.subIncludedDataBalance = getIncludedDataBalance; }
    if ([getRollOverDataBalance isKindOfClass:[NSNull class]]) { self.subRollOverDataBalance = @"0.0"; } else { self.subRollOverDataBalance = getRollOverDataBalance; }
    if ([getRollOverCreditBalance isKindOfClass:[NSNull class]]) { self.subRollOverCreditBalance = @"0.0"; } else { self.subRollOverCreditBalance = getRollOverCreditBalance; }
    if ([getInternationalTalkBalance isKindOfClass:[NSNull class]]) { self.subInternationalTalkBalance = @"0.0"; } else { self.subInternationalTalkBalance = getInternationalTalkBalance; }
    self.subExpiryDate = [[[array objectAtIndex:1] objectForKey:@"attributes"] objectForKey:fieldSubscriptionExpiryDate];

    return self;
}

@end
