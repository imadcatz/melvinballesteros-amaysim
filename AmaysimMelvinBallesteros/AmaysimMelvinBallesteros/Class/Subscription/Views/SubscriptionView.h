//
//  SubscriptionView.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubscriptionModel.h"
#import "Helper+Labels.h"
#import "Helper+Conversion.h"

@interface SubscriptionView : UIView {
    
    UILabel *lblHeaderAutoRenewal;
    UILabel *lblHeaderAutoRenewalValue;
    UILabel *lblHeaderPrimarySubscription;
    UILabel *lblHeaderPrimarySubscriptionValue;
    UILabel *lblHeaderIncludedDataBalance;
    UILabel *lblHeaderIncludedDataBalanceValue;
    UILabel *lblHeaderIncludedCreditBalance;
    UILabel *lblHeaderIncludedCreditBalanceValue;
    UILabel *lblHeaderRolloverCreditBalance;
    UILabel *lblHeaderRolloverCreditBalanceValue;
    UILabel *lblHeaderRolloverDataBalance;
    UILabel *lblHeaderRolloverDataBalanceValue;
    UILabel *lblHeaderInternationalTalkBalance;
    UILabel *lblHeaderInternationalTalkBalanceValue;
    UILabel *lblHeaderExpiration;
    UILabel *lblHeaderExpirationValue;

    SubscriptionModel *subscriptionModel;
    
}



-(id)initWithFrame:(CGRect)frame model:(SubscriptionModel *)model;

@property (nonatomic, strong) UIButton *btnBack;
@property (nonatomic, strong) UIButton *btnProducts;

@end
