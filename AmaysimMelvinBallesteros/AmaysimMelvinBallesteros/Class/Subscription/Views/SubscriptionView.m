//
//  SubscriptionView.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "SubscriptionView.h"

@implementation SubscriptionView

-(id)initWithFrame:(CGRect)frame model:(SubscriptionModel *)model {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        subscriptionModel = model;
        [self initCreateLabels];
        [self initCreateBackButton];
    }
    return self;
}

-(void)initCreateLabels {
    lblHeaderAutoRenewal = [Helper_Labels helperLabelHeader:CGRectMake(0, 30, 200, 30) text:@"Auto Renewal"];
    [lblHeaderAutoRenewal setFrame:CGRectMake((screenWidth-lblHeaderAutoRenewal.frame.size.width)/2, lblHeaderAutoRenewal.frame.origin.y, lblHeaderAutoRenewal.frame.size.width, lblHeaderAutoRenewal.frame.size.height)];
    [self addSubview:lblHeaderAutoRenewal];
    
    lblHeaderAutoRenewalValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderAutoRenewal.frame)-2, 200, 30) text:subscriptionModel.subAutoRenewal];
    [lblHeaderAutoRenewalValue setFrame:CGRectMake((screenWidth-lblHeaderAutoRenewalValue.frame.size.width)/2, lblHeaderAutoRenewalValue.frame.origin.y, lblHeaderAutoRenewalValue.frame.size.width, lblHeaderAutoRenewalValue.frame.size.height)];
    [self addSubview:lblHeaderAutoRenewalValue];
    
    //-->>PRIMARY SUBSCRIPTION
    lblHeaderPrimarySubscription = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderAutoRenewalValue.frame) + 5, 200, 30) text:@"Primary Subscription"];
    [lblHeaderPrimarySubscription setFrame:CGRectMake((screenWidth-lblHeaderPrimarySubscription.frame.size.width)/2, lblHeaderPrimarySubscription.frame.origin.y, lblHeaderPrimarySubscription.frame.size.width, lblHeaderPrimarySubscription.frame.size.height)];
    [self addSubview:lblHeaderPrimarySubscription];
    
    lblHeaderPrimarySubscriptionValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderPrimarySubscription.frame)-2, 200, 30) text:subscriptionModel.subPrimary];
    [lblHeaderPrimarySubscriptionValue setFrame:CGRectMake((screenWidth-lblHeaderPrimarySubscriptionValue.frame.size.width)/2, lblHeaderPrimarySubscriptionValue.frame.origin.y, lblHeaderPrimarySubscriptionValue.frame.size.width, lblHeaderPrimarySubscriptionValue.frame.size.height)];
    [self addSubview:lblHeaderPrimarySubscriptionValue];
    
    //-->>INCLUDED DATA BALANCE
    lblHeaderIncludedDataBalance = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderPrimarySubscriptionValue.frame) + 5, 200, 30) text:@"Included Data Balance"];
    [lblHeaderIncludedDataBalance setFrame:CGRectMake((screenWidth-lblHeaderIncludedDataBalance.frame.size.width)/2, lblHeaderIncludedDataBalance.frame.origin.y, lblHeaderIncludedDataBalance.frame.size.width, lblHeaderIncludedDataBalance.frame.size.height)];
    [self addSubview:lblHeaderIncludedDataBalance];

    NSString *dataBal = subscriptionModel.subIncludedDataBalance;
    double getIncludedDataBalance = [dataBal doubleValue];
    lblHeaderIncludedDataBalanceValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderIncludedDataBalance.frame)-2, 200, 30) text:[Helper_Conversion helperConvertMegaToGiga:getIncludedDataBalance]];
    [lblHeaderIncludedDataBalanceValue setFrame:CGRectMake((screenWidth-lblHeaderIncludedDataBalanceValue.frame.size.width)/2, lblHeaderIncludedDataBalanceValue.frame.origin.y, lblHeaderIncludedDataBalanceValue.frame.size.width, lblHeaderIncludedDataBalanceValue.frame.size.height)];
    [self addSubview:lblHeaderIncludedDataBalanceValue];
    
    //-->>INCLUDED CREDIT BALANCE
    lblHeaderIncludedCreditBalance = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderIncludedDataBalanceValue.frame) + 5, 200, 30) text:@"Included Credit Balance"];
    [lblHeaderIncludedCreditBalance setFrame:CGRectMake((screenWidth-lblHeaderIncludedCreditBalance.frame.size.width)/2, lblHeaderIncludedCreditBalance.frame.origin.y, lblHeaderIncludedCreditBalance.frame.size.width, lblHeaderIncludedCreditBalance.frame.size.height)];
    [self addSubview:lblHeaderIncludedCreditBalance];
    
    lblHeaderIncludedCreditBalanceValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderIncludedCreditBalance.frame)-2, 200, 30) text:subscriptionModel.subIncludedCreditBalance];
    [lblHeaderIncludedCreditBalanceValue setFrame:CGRectMake((screenWidth-lblHeaderIncludedCreditBalanceValue.frame.size.width)/2, lblHeaderIncludedCreditBalanceValue.frame.origin.y, lblHeaderIncludedCreditBalanceValue.frame.size.width, lblHeaderIncludedCreditBalanceValue.frame.size.height)];
    [self addSubview:lblHeaderIncludedCreditBalanceValue];
    
    //-->>ROLLOVER CREDIT
    lblHeaderRolloverCreditBalance = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderIncludedDataBalanceValue.frame) + 5, 200, 30) text:@"Included Rolloever Credit Balance"];
    [lblHeaderRolloverCreditBalance setFrame:CGRectMake((screenWidth-lblHeaderRolloverCreditBalance.frame.size.width)/2, lblHeaderRolloverCreditBalance.frame.origin.y, lblHeaderRolloverCreditBalance.frame.size.width, lblHeaderRolloverCreditBalance.frame.size.height)];
    [self addSubview:lblHeaderRolloverCreditBalance];
    
    lblHeaderRolloverCreditBalanceValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderRolloverCreditBalance.frame)-2, 200, 30) text:subscriptionModel.subRollOverCreditBalance];
    [lblHeaderRolloverCreditBalanceValue setFrame:CGRectMake((screenWidth-lblHeaderRolloverCreditBalanceValue.frame.size.width)/2, lblHeaderRolloverCreditBalanceValue.frame.origin.y, lblHeaderRolloverCreditBalanceValue.frame.size.width, lblHeaderRolloverCreditBalanceValue.frame.size.height)];
    [self addSubview:lblHeaderRolloverCreditBalanceValue];
    
    //-->>ROLLOVER DATA
    lblHeaderRolloverDataBalance = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderRolloverCreditBalanceValue.frame) + 5, 200, 30) text:@"Included Rolloever Data Balance"];
    [lblHeaderRolloverDataBalance setFrame:CGRectMake((screenWidth-lblHeaderRolloverDataBalance.frame.size.width)/2, lblHeaderRolloverDataBalance.frame.origin.y, lblHeaderRolloverDataBalance.frame.size.width, lblHeaderRolloverDataBalance.frame.size.height)];
    [self addSubview:lblHeaderRolloverDataBalance];
    
    lblHeaderRolloverDataBalanceValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderRolloverDataBalance.frame)-2, 200, 30) text:subscriptionModel.subRollOverDataBalance];
    [lblHeaderRolloverDataBalanceValue setFrame:CGRectMake((screenWidth-lblHeaderRolloverDataBalanceValue.frame.size.width)/2, lblHeaderRolloverDataBalanceValue.frame.origin.y, lblHeaderRolloverDataBalanceValue.frame.size.width, lblHeaderRolloverDataBalanceValue.frame.size.height)];
    [self addSubview:lblHeaderRolloverDataBalanceValue];
    
    //-->>INTERNATIONAL TALK
    lblHeaderInternationalTalkBalance = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderRolloverDataBalanceValue.frame) + 5, 200, 30) text:@"Included International Talk Balance"];
    [lblHeaderInternationalTalkBalance setFrame:CGRectMake((screenWidth-lblHeaderInternationalTalkBalance.frame.size.width)/2, lblHeaderInternationalTalkBalance.frame.origin.y, lblHeaderInternationalTalkBalance.frame.size.width, lblHeaderInternationalTalkBalance.frame.size.height)];
    [self addSubview:lblHeaderInternationalTalkBalance];
    
    lblHeaderInternationalTalkBalanceValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderInternationalTalkBalance.frame)-2, 200, 30) text:subscriptionModel.subInternationalTalkBalance];
    [lblHeaderInternationalTalkBalanceValue setFrame:CGRectMake((screenWidth-lblHeaderInternationalTalkBalanceValue.frame.size.width)/2, lblHeaderInternationalTalkBalanceValue.frame.origin.y, lblHeaderInternationalTalkBalanceValue.frame.size.width, lblHeaderInternationalTalkBalanceValue.frame.size.height)];
    [self addSubview:lblHeaderInternationalTalkBalanceValue];
    
    //-->>EXPIRATION DATE
    lblHeaderExpiration = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderInternationalTalkBalanceValue.frame) + 5, 200, 30) text:@"Expiration Date"];
    [lblHeaderExpiration setFrame:CGRectMake((screenWidth-lblHeaderExpiration.frame.size.width)/2, lblHeaderExpiration.frame.origin.y, lblHeaderExpiration.frame.size.width, lblHeaderExpiration.frame.size.height)];
    [self addSubview:lblHeaderExpiration];
    
    lblHeaderExpirationValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderExpiration.frame)-2, 200, 30) text:subscriptionModel.subExpiryDate];
    [lblHeaderExpirationValue setFrame:CGRectMake((screenWidth-lblHeaderExpirationValue.frame.size.width)/2, lblHeaderExpirationValue.frame.origin.y, lblHeaderExpirationValue.frame.size.width, lblHeaderExpirationValue.frame.size.height)];
    [self addSubview:lblHeaderExpirationValue];
    
}

-(void)initCreateBackButton {
    self.btnBack = [[UIButton alloc] initWithFrame:CGRectMake(((screenWidth-80-62-10)/2), CGRectGetMaxY(lblHeaderExpirationValue.frame) + 30, 80, 75)];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
    [self addSubview:self.btnBack];
    
    self.btnProducts = [[UIButton alloc] initWithFrame:CGRectMake(((screenWidth-62)/2)+35, CGRectGetMaxY(lblHeaderExpirationValue.frame) + 32, 62, 62)];
    [self.btnProducts setBackgroundImage:[UIImage imageNamed:@"icon-product"] forState:UIControlStateNormal];
    [self addSubview:self.btnProducts];
}

@end
