//
//  SubscriptionViewController.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubscriptionService.h"
#import "Helper+LayerGradient.h"
#import "Helper+Labels.h"
#import "SubscriptionView.h"
#import "SubscriptionModel.h"
#import "Helper+Json.h"
#import "ProductsViewController.h"

@interface SubscriptionViewController : UIViewController {
    UIImageView *imgViewAppLogo;
    UILabel *lblHeader;
    
    SubscriptionView *subscriptionView;
}
@property (nonatomic, strong) UIScrollView *scrollView;

@end
