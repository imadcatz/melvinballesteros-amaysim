//
//  LoginViewController.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@end

@implementation LoginViewController

#pragma mark - View Delegate
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initCustomBackground];
    [self initNavControllerHide];
    [self initCreateLogo];
    [self initViewControls];
}


#pragma mark - Initialization
-(void)initCustomBackground {
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

-(void)initNavControllerHide {
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)initCreateLogo {
    imgViewAppLogo = [[UIImageView alloc] initWithFrame:CGRectMake((screenWidth-186.5)/2, (screenHeight-40)/2, 186.5, 40)];
    [imgViewAppLogo setImage:[UIImage imageNamed:@"AppLogo"]];
    [self.view addSubview:imgViewAppLogo];
}

-(void)initViewControls {
    [self performSelector:@selector(actionAnimateLogoDisplay) withObject:nil afterDelay:2.5];
    [self performSelector:@selector(actionAnimateDisplayTextField) withObject:nil afterDelay:3];
}


#pragma mark - Action Delegate
-(void)actionAnimateLogoDisplay {
    [LoginService serviceLoginAnimateLogo:imgViewAppLogo];
}

-(void)actionAnimateDisplayTextField {
    viewLoginControls = [[LoginControlsView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imgViewAppLogo.frame) + 20, screenWidth, 300)];
    [viewLoginControls.btnLogin addTarget:self action:@selector(actionLoginAuthentication) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:viewLoginControls];
}

-(void)actionLoginAuthentication {
    [self.view endEditing:YES];
    [Helper_Overlay helperOverlayStart];
    if ([LoginService serviceloginAuthenticateUsername:viewLoginControls.txtEmail.text password:viewLoginControls.txtPassword.text]) {
        [self performSelector:@selector(initDisplayControllerAccount) withObject:nil afterDelay:2];     
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Incorrect Username or Password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
        [Helper_Overlay helperOverlayStop];
    }
}

-(void)initDisplayControllerAccount {
    [Helper_Overlay helperOverlayStop];
    self.controllerAccount = [[AccountViewController alloc] init];
    [self.navigationController pushViewController:self.controllerAccount animated:YES];
}





#pragma mark - Memory Management
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
