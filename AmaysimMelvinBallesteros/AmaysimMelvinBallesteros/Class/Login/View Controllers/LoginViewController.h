//
//  LoginViewController.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginService.h"
#import "LoginControlsView.h"
#import "AccountViewController.h"
#import "Helper+Overlay.h"

@interface LoginViewController : UIViewController {
    
    UIImageView *imgViewAppLogo;
    LoginControlsView *viewLoginControls;
    
}

@property (nonatomic, strong) AccountViewController *controllerAccount;

@end
