//
//  LoginControlsView.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "LoginControlsView.h"

@implementation LoginControlsView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self initCreateLabelTextField];
    }
    return self;
}

-(void)initCreateLabelTextField {
    
    //-->>CREATE LABEL EMAIL
    self.lblEmail = [Helper_Labels helperLabelLoginHeader:CGRectMake(29, 28, 250, 30) text:@"Email"];
    [self addSubview:self.lblEmail];
    
    self.lblPassword = [Helper_Labels helperLabelLoginHeader:CGRectMake(29, CGRectGetMaxY(self.lblEmail.frame) + 20 , 250, 30) text:@"Password"];
    [self addSubview:self.lblPassword];
    
    //-->>CREATE UITEXTFIELD
    self.txtEmail = [Helper_TextField helperTextFieldLoginEmail:CGRectMake(15, 18, screenWidth-30, 35)];
    [self.txtEmail setTag:0];
    [self.txtEmail setDelegate:self];
    [self addSubview:self.txtEmail];
    
    self.txtPassword = [Helper_TextField helperTextFieldLoginPassword:CGRectMake(15, CGRectGetMaxY(self.txtEmail.frame) + 2, screenWidth-30, 35)];
    [self.txtPassword setTag:1];
    [self.txtPassword setDelegate:self];
    [self addSubview:self.txtPassword];
    
    //-->>CREATE HORIZONTAL LINE BELOW TEXTFIELD
    CAShapeLayer *lineEmailLayer = [Helper_Lines helperCreateHorizontalLine:CGPointMake(30,CGRectGetMaxY(self.lblEmail.frame)) lineToPoint:CGPointMake(screenWidth-30, CGRectGetMaxY(self.lblEmail.frame))];
    [self.layer addSublayer:lineEmailLayer];
    
    CAShapeLayer *linePasswordLayer = [Helper_Lines helperCreateHorizontalLine:CGPointMake(30,CGRectGetMaxY(self.lblPassword.frame)) lineToPoint:CGPointMake(screenWidth-30, CGRectGetMaxY(self.lblPassword.frame))];
    [self.layer addSublayer:linePasswordLayer];
    
    self.btnLogin = [Helper_Buttons helperCreateButtonLogin:CGRectMake(30, CGRectGetMaxY(self.txtPassword.frame)+ 20, (screenWidth-60), 40) text:@"LOGIN"];
    [self addSubview:self.btnLogin];
    
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 0) {
        if ([textField.text length] > 0) {
        } else if ([textField.text length] == 0) {
            [Helper_Animation animateLabelShrink:self.lblEmail originX:29];
        }
    }
    
    //Text Password
    if (textField.tag == 1) {
        if ([textField.text length] > 0) {
        } else if ([textField.text length] == 0) {
            [Helper_Animation animateLabelShrink:self.lblPassword originX:29];
            
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 0) {
        if ([textField.text length] > 0) {
        } else if ([textField.text length] == 0) {
            [Helper_Animation aniemateLabelDefault:self.lblEmail originX:29];
        }
    }
    
    //Text Password
    if (textField.tag == 1) {
        if ([textField.text length] > 0) {
        } else if ([textField.text length] == 0) {
            [Helper_Animation aniemateLabelDefault:self.lblPassword originX:29];
        }
    }
}



@end
