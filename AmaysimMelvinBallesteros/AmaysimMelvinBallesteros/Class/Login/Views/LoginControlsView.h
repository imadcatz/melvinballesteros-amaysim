//
//  LoginControlsView.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper+TextField.h"
#import "Helper+Labels.h"
#import "Helper+Lines.h"
#import "Helper+Buttons.h"
#import "Helper+Animation.h"

@interface LoginControlsView : UIView <UITextFieldDelegate>

-(id)initWithFrame:(CGRect)frame;


@property (nonatomic, strong) UILabel *lblEmail;
@property (nonatomic, strong) UILabel *lblPassword;
@property (nonatomic, strong) UITextField *txtEmail;
@property (nonatomic, strong) UITextField *txtPassword;
@property (nonatomic, strong) UIButton *btnLogin;

@end
