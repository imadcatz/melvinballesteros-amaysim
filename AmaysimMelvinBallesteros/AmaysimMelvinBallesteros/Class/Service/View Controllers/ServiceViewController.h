//
//  ServiceViewController.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper+LayerGradient.h"
#import "Helper+Labels.h"
#import "ServiceView.h"
#import "Helper+Json.h"

@interface ServiceViewController : UIViewController {
    UIImageView *imgViewAppLogo;   
    UILabel *lblHeader;
    ServiceView *serviceView;
}

@property (nonatomic, strong) UIScrollView *scrollView;

@end
