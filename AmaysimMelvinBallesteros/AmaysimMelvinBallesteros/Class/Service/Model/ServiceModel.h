//
//  ServiceModel.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

extern NSString *const fieldServiceMSN;
extern NSString *const fieldServiceCreditExpiration;
extern NSString *const fieldServiceCredit;
extern NSString *const fieldServiceDataUsageThreshold;

#import <Foundation/Foundation.h>

@interface ServiceModel : NSObject

-(id)initWithDictionary:(NSArray *)array;

@property (nonatomic, strong) NSString *serviceMSN;
@property (nonatomic, strong) NSString *serviceCredit;
@property (nonatomic, strong) NSString *serviceCreditExpiration;
@property (nonatomic, strong) NSString *serviceDataThreshold;

@end
