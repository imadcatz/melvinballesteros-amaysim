//
//  ServiceModel.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

NSString *const fieldServiceMSN = @"msn";
NSString *const fieldServiceCreditExpiration = @"credit-expiry";
NSString *const fieldServiceCredit = @"credit";
NSString *const fieldServiceDataUsageThreshold = @"data-usage-threshold";

#import "ServiceModel.h"

@implementation ServiceModel

-(id)initWithDictionary:(NSArray *)array {
    self.serviceMSN = [[[array objectAtIndex:0] objectForKey:@"attributes"] objectForKey:fieldServiceMSN];
    self.serviceCredit = [[[array objectAtIndex:0] objectForKey:@"attributes"] objectForKey:fieldServiceCredit];
    self.serviceCreditExpiration = [[[array objectAtIndex:0] objectForKey:@"attributes"] objectForKey:fieldServiceCreditExpiration];
    self.serviceDataThreshold = [[[array objectAtIndex:0] objectForKey:@"attributes"] objectForKey:fieldServiceDataUsageThreshold];

    return self;
}

@end
