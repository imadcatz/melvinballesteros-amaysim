//
//  ServiceView.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceModel.h"
#import "Helper+Labels.h"
#import "Helper+Conversion.h"


@interface ServiceView : UIView {
    UILabel *lblHeaderMSN;
    UILabel *lblHeaderMSNValue;
    UILabel *lblHeaderCredit;
    UILabel *lblHeaderCreditValues;
    UILabel *lblHeaderCreditExpiration;
    UILabel *lblHeaderCreditExpirationValue;
    UILabel *lblHeaderDataUsageThreshold;
    UILabel *lblHeaderDataUsageThresholdValue;
    
    ServiceModel *serviceModel;
}

-(id)initWithFrame:(CGRect)frame model:(ServiceModel *)model;

@property (nonatomic, strong) UIButton *btnBack;

@end
