//
//  ServiceView.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "ServiceView.h"

@implementation ServiceView

-(id)initWithFrame:(CGRect)frame model:(ServiceModel *)model {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        serviceModel = model;
        [self initCreateLabels];
        [self initCreateBackButton];
    }
    return self;
}

-(void)initCreateLabels {
    lblHeaderMSN = [Helper_Labels helperLabelHeader:CGRectMake(0, 30, 200, 30) text:@"MSN"];
    [lblHeaderMSN setFrame:CGRectMake((screenWidth-lblHeaderMSN.frame.size.width)/2, lblHeaderMSN.frame.origin.y, lblHeaderMSN.frame.size.width, lblHeaderMSN.frame.size.height)];
    [self addSubview:lblHeaderMSN];
    
    lblHeaderMSNValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderMSN.frame)-2, 200, 30) text:serviceModel.serviceMSN];
    [lblHeaderMSNValue setFrame:CGRectMake((screenWidth-lblHeaderMSNValue.frame.size.width)/2, lblHeaderMSNValue.frame.origin.y, lblHeaderMSNValue.frame.size.width, lblHeaderMSNValue.frame.size.height)];
    [self addSubview:lblHeaderMSNValue];
    
    lblHeaderCredit = [Helper_Labels helperLabelHeader:CGRectMake(0, 30, 200, 30) text:@"CREDIT"];
    [lblHeaderCredit setFrame:CGRectMake((screenWidth-lblHeaderCredit.frame.size.width)/2,CGRectGetMaxY(lblHeaderMSNValue.frame) + 5, lblHeaderCredit.frame.size.width, lblHeaderCredit.frame.size.height)];
    [self addSubview:lblHeaderCredit];
    
    double getValue = [serviceModel.serviceCredit doubleValue];
    lblHeaderCreditValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderCredit.frame)-2, 200, 30) text:[Helper_Conversion helperConvertMegaToGiga:getValue]];
    [lblHeaderCreditValues setFrame:CGRectMake((screenWidth-lblHeaderCreditValues.frame.size.width)/2, lblHeaderCreditValues.frame.origin.y, lblHeaderCreditValues.frame.size.width, lblHeaderCreditValues.frame.size.height)];
    [self addSubview:lblHeaderCreditValues];
    
    lblHeaderCreditExpiration = [Helper_Labels helperLabelHeader:CGRectMake(0, CGRectGetMaxY(lblHeaderCreditValues.frame) + 5, 200, 30) text:@"CREDIT EXPIRATION"];
    [lblHeaderCreditExpiration setFrame:CGRectMake((screenWidth-lblHeaderCreditExpiration.frame.size.width)/2, lblHeaderCreditExpiration.frame.origin.y, lblHeaderCreditExpiration.frame.size.width, lblHeaderCreditExpiration.frame.size.height)];
    [self addSubview:lblHeaderCreditExpiration];
    
    lblHeaderCreditExpirationValue = [Helper_Labels helperLabelHeaderValues:CGRectMake(0, CGRectGetMaxY(lblHeaderCreditExpiration.frame)-2, 200, 30) text:serviceModel.serviceCreditExpiration];
    [lblHeaderCreditExpirationValue setFrame:CGRectMake((screenWidth-lblHeaderCreditExpirationValue.frame.size.width)/2, lblHeaderCreditExpirationValue.frame.origin.y, lblHeaderCreditExpirationValue.frame.size.width, lblHeaderCreditExpirationValue.frame.size.height)];
    [self addSubview:lblHeaderCreditExpirationValue];
}

-(void)initCreateBackButton {
    self.btnBack = [[UIButton alloc] initWithFrame:CGRectMake((screenWidth-80)/2, CGRectGetMaxY(lblHeaderCreditExpirationValue.frame) + 30, 80, 75)];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
    [self addSubview:self.btnBack];
}

@end
