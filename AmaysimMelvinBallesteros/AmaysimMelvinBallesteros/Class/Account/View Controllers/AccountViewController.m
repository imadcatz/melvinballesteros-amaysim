//
//  AccountViewController.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "AccountViewController.h"

@interface AccountViewController ()

@end

@implementation AccountViewController

#pragma mark - View Delegate
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavControllerHide];
    [self initCreateGradientBackground];
    [self initCreateHeaderTitle];
    [self initCreateScrollView];
    [self initCreateAccountViewContents];
}

#pragma mark - Initialization
-(void)initNavControllerHide {
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)initCreateGradientBackground {
    [self.view.layer addSublayer:[Helper_LayerGradient helperGradientLinearBackground:UIColorFromRGB(0xE65013) color2:UIColorFromRGB(0xffffff) view:self.view]];
    [self.view.layer setShouldRasterize:YES];
    [self.view.layer setRasterizationScale:[UIScreen mainScreen].scale];
}

-(void)initCreateHeaderTitle {
    lblHeader = [Helper_Labels helperLabelTitleHeader:CGRectMake((screenWidth-250)/2, 25, 250, 44) text:@"Account Information"];
    [self.view addSubview:lblHeader];
}

-(void)initCreateScrollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lblHeader.frame), screenWidth, screenHeight-CGRectGetMaxY(lblHeader.frame))];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setContentSize:CGSizeMake(screenWidth, self.scrollView.frame.size.height + 1)];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    [self.scrollView setDelaysContentTouches:NO];
    [self.view addSubview:self.scrollView];
}

-(void)initCreateAccountViewContents {
    AccountModel *model = [AccountService initCreateAccountModel];
    accountView = [[AccountView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, self.scrollView.frame.size.height) model:model];
    [accountView.btnSubscription addTarget:self action:@selector(initDisplayControllerSubscription) forControlEvents:UIControlEventTouchUpInside];
    [accountView.btnServices addTarget:self action:@selector(initDisplayControllerService) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:accountView];
}

-(void)initDisplayControllerSubscription {
    SubscriptionViewController *controllerSubscription = [[SubscriptionViewController alloc] init];
    [self.navigationController pushViewController:controllerSubscription animated:YES];
}

-(void)initDisplayControllerService {
    ServiceViewController *controllerService = [[ServiceViewController alloc] init];
    [self.navigationController pushViewController:controllerService animated:YES];
}




#pragma mark - Memory Management
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
