//
//  AccountViewController.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper+LayerGradient.h"
#import "Helper+Labels.h"
#import "AccountService.h"
#import "AccountView.h"
#import "SubscriptionViewController.h"
#import "ServiceViewController.h"

@interface AccountViewController : UIViewController {
    UILabel *lblHeader;
    AccountView *accountView;
}

@property (nonatomic, strong) UIScrollView * scrollView;

@end
