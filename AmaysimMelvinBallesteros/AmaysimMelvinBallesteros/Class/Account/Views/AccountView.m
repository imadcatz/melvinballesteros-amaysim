//
//  AccountView.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "AccountView.h"

@implementation AccountView

-(id)initWithFrame:(CGRect)frame model:(AccountModel *)model {
    self = [super initWithFrame:frame];
    if (self) {
        accountModel = model;
        [self initCreateLabels];
        [self initCreateButtons];
    }
    return self;
}

-(void)initCreateLabels {
    lblHeaderAccountID = [Helper_Labels helperLabelHeader:CGRectMake(30, 30, 200, 30) text:@"Account No"];
    [self addSubview:lblHeaderAccountID];
    
    lblHeaderAccountIDValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(30, CGRectGetMaxY(lblHeaderAccountID.frame)-2, 200, 30) text:accountModel.accountID];
    [self addSubview:lblHeaderAccountIDValues];
    
    lblHeaderName = [Helper_Labels helperLabelHeader:CGRectMake(30, CGRectGetMaxY(lblHeaderAccountIDValues.frame)+ 5, 200, 30) text:@"Account Name"];
    [self addSubview:lblHeaderName];
    
    NSString *getAccountName = [NSString stringWithFormat:@"%@. %@ %@", accountModel.accountTitle, accountModel.accountFirstName, accountModel.accountLastName];
    lblHeaderNameValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(30, CGRectGetMaxY(lblHeaderName.frame)-2, 200, 30) text:getAccountName];
    [self addSubview:lblHeaderNameValues];
    
    lblHeaderEmail = [Helper_Labels helperLabelHeader:CGRectMake(30, CGRectGetMaxY(lblHeaderNameValues.frame)+ 5, 200, 30) text:@"Account Email"];
    [self addSubview:lblHeaderEmail];
    
    lblHeaderEmailValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(30, CGRectGetMaxY(lblHeaderEmail.frame)-2, 200, 30) text:accountModel.accountEmail];
    [self addSubview:lblHeaderEmailValues];
    
    lblHeaderBirthDate = [Helper_Labels helperLabelHeader:CGRectMake(30, CGRectGetMaxY(lblHeaderEmailValues.frame)+ 5, 200, 30) text:@"Birth Date"];
    [self addSubview:lblHeaderBirthDate];
    
    lblHeaderBirthDateValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(30, CGRectGetMaxY(lblHeaderBirthDate.frame)-2, 200, 30) text:accountModel.accountBirthDate];
    [self addSubview:lblHeaderBirthDateValues];
    
    lblHeaderPaymentType = [Helper_Labels helperLabelHeader:CGRectMake(30, CGRectGetMaxY(lblHeaderBirthDateValues.frame)+ 5, 200, 30) text:@"Payment Type"];
    [self addSubview:lblHeaderPaymentType];
    
    lblHeaderPaymentTypeValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(30, CGRectGetMaxY(lblHeaderPaymentType.frame)-2, 200, 30) text:accountModel.accountPaymentType];
    [self addSubview:lblHeaderPaymentTypeValues];
    
    lblHeaderContactNo = [Helper_Labels helperLabelHeader:CGRectMake(30, CGRectGetMaxY(lblHeaderPaymentTypeValues.frame)+ 5, 200, 30) text:@"Contact Number"];
    [self addSubview:lblHeaderContactNo];
    
    lblHeaderContactNoValues = [Helper_Labels helperLabelHeaderValues:CGRectMake(30, CGRectGetMaxY(lblHeaderContactNo.frame)-2, 200, 30) text:accountModel.accountContactNo];
    [self addSubview:lblHeaderContactNoValues];
}

-(void)initCreateButtons {
    self.btnSubscription = [Helper_Buttons helperCreateButtonLogin:CGRectMake(30, CGRectGetMaxY(lblHeaderContactNoValues.frame) + 30, (screenWidth-60), 40) text:@"View Subscriptions"];
    [self addSubview:self.btnSubscription];
    
    self.btnServices = [Helper_Buttons helperCreateButtonLogin:CGRectMake(30, CGRectGetMaxY(self.btnSubscription.frame) + 10, (screenWidth-60), 40) text:@"View Services"];
    [self addSubview:self.btnServices];

}

@end
