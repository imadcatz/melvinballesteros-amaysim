//
//  AccountView.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountModel.h"
#import "Helper+Labels.h"
#import "Helper+Lines.h"
#import "Helper+Buttons.h"

@interface AccountView : UIView {
    AccountModel *accountModel;
    
    UILabel *lblHeaderName;
    UILabel *lblHeaderNameValues;
    UILabel *lblHeaderAccountID;
    UILabel *lblHeaderAccountIDValues;
    UILabel *lblHeaderBirthDate;
    UILabel *lblHeaderBirthDateValues;
    UILabel *lblHeaderEmail;
    UILabel *lblHeaderEmailValues;
    UILabel *lblHeaderEmailSubscription;
    UILabel *lblHeaderEmailSubscriptionValues;
    UILabel *lblHeaderPaymentType;
    UILabel *lblHeaderPaymentTypeValues;
    UILabel *lblHeaderContactNo;
    UILabel *lblHeaderContactNoValues;

}

-(id)initWithFrame:(CGRect)frame model:(AccountModel *)model;
@property (nonatomic, strong) UIButton *btnSubscription;
@property (nonatomic, strong) UIButton *btnServices;

@end
