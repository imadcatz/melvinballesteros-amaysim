//
//  AccountModel.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

extern NSString *const fieldAccountFirstName;
extern NSString *const fieldAccountLastName;
extern NSString *const fieldAccountTitle;
extern NSString *const fieldAccountEmail;
extern NSString *const fieldAccountContactNo;
extern NSString *const fieldAccountBirthDate;
extern NSString *const fieldAccountID;
extern NSString *const fieldAccountPaymentType;

#import <Foundation/Foundation.h>

@interface AccountModel : NSObject

-(id)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, strong) NSString *accountID;
@property (nonatomic, strong) NSString *accountFirstName;
@property (nonatomic, strong) NSString *accountLastName;
@property (nonatomic, strong) NSString *accountEmail;
@property (nonatomic, strong) NSString *accountPaymentType;
@property (nonatomic, strong) NSString *accountContactNo;
@property (nonatomic, strong) NSString *accountTitle;
@property (nonatomic, strong) NSString *accountBirthDate;


@end
