//
//  AccountModel.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//


#pragma mark - Fieldname Accounts
NSString *const fieldAccountFirstName = @"first-name";
NSString *const fieldAccountLastName = @"last-name";
NSString *const fieldAccountTitle = @"title";
NSString *const fieldAccountEmail = @"email-address";
NSString *const fieldAccountContactNo = @"contact-number";
NSString *const fieldAccountBirthDate = @"date-of-birth";
NSString *const fieldAccountID = @"id";
NSString *const fieldAccountPaymentType = @"payment-type";

#import "AccountModel.h"

@implementation AccountModel
-(id)initWithDictionary:(NSDictionary *)dictionary {
    self.accountID = [dictionary objectForKey:fieldAccountID];
    self.accountLastName = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountLastName];
    self.accountFirstName = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountFirstName];
    self.accountEmail = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountEmail];
    self.accountBirthDate = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountBirthDate];
    self.accountContactNo = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountContactNo];
    self.accountTitle = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountTitle];
    self.accountPaymentType = [[dictionary objectForKey:@"attributes"] objectForKey:fieldAccountPaymentType];

    return self;
}

@end
