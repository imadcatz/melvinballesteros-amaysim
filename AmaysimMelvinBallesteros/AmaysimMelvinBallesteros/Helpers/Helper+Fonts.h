//
//  Helper+Fonts.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper_Fonts : NSObject

+(NSString *)fontAppleGothicRegular;
+(NSString *)fontAppleGothicBold;
+(NSString *)fontAppleGothicMedium;
+(NSString *)fontAppleGothicLight;

@end
