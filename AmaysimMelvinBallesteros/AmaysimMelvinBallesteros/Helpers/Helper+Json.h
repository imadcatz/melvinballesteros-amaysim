//
//  Helper+Json.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper_Json : NSObject

+(NSDictionary *)helperGetJsonFile;
+(NSDictionary *)helperGetAccounts:(NSDictionary *)dictionary;
+(NSArray *)helperGetServices:(NSDictionary *)dictionary;
+(NSArray *)helperGetSubscription:(NSDictionary *)dictionary;
+(NSArray *)helperGetProducts:(NSDictionary *)dictionary;

@end
