//
//  Helper+Animation.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper_Animation : NSObject

+(void)animateLabelShrink:(UILabel *)label originX:(CGFloat)originX;
+(void)aniemateLabelDefault:(UILabel *)label originX:(CGFloat)originX;


@end
