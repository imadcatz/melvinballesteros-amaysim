//
//  Helper+Animation.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Animation.h"

@implementation Helper_Animation

+(void)animateLabelShrink:(UILabel *)label originX:(CGFloat)originX {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDelegate:self];
    
    label.transform = CGAffineTransformMakeScale(0.6, 0.6);     //shrink
    CGRect frame = label.frame;
    frame.origin.x = originX;
    frame.origin.y = label.frame.origin.y - 17;
    [label setFrame:frame];
    //[label setTextColor:[UIColor darkGrayColor]];
    [label setTextColor:UIColorFromRGB(0xF08E5C)];
    
    [UIView commitAnimations];
}

+(void)aniemateLabelDefault:(UILabel *)label originX:(CGFloat)originX {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDelegate:self];
    
    label.transform = CGAffineTransformMakeScale(1.0, 1.0);     //shrink
    CGRect frame = label.frame;
    frame.origin.x = originX;
    frame.origin.y = label.frame.origin.y + 17;
    [label setFrame:frame];
    [label setTextColor:UIColorFromRGB(0xE65013)];
    //[label setTextColor:[UIColorFromRGB(0xE65013) colorWithAlphaComponent:0.50]];
    
    [UIView commitAnimations];
}

@end
