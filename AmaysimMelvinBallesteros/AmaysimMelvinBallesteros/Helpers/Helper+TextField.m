//
//  Helper+TextField.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+TextField.h"

@implementation Helper_TextField

+(UITextField *)helperTextFieldLoginEmail:(CGRect)frame {
    UITextField *txtFieldEmail = [[UITextField alloc] initWithFrame:frame];
    [txtFieldEmail setBackgroundColor:[UIColor clearColor]];
    [txtFieldEmail setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicRegular] size:15]];
    [txtFieldEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtFieldEmail setTextColor:UIColorFromRGB(0xE65013)];
    [txtFieldEmail setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, 15, 15)];
    txtFieldEmail.leftView = paddingView;
    txtFieldEmail.leftViewMode = UITextFieldViewModeAlways;
    
    return txtFieldEmail;
}


+(UITextField *)helperTextFieldLoginPassword:(CGRect)frame {
    UITextField *txtFieldPassword = [[UITextField alloc] initWithFrame:frame];
    [txtFieldPassword setBackgroundColor:[UIColor clearColor]];
    [txtFieldPassword setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicBold] size:15]];
    [txtFieldPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtFieldPassword setTextColor:UIColorFromRGB(0xE65013)];
    [txtFieldPassword setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, 15, 15)];
    txtFieldPassword.leftView = paddingView;
    txtFieldPassword.leftViewMode = UITextFieldViewModeAlways;
    [txtFieldPassword setSecureTextEntry:YES];
    
    return txtFieldPassword;
}

@end
