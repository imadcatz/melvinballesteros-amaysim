//
//  Helper+Overlay.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OverlayView.h"
#import "AmaysimApplication.h"

@interface Helper_Overlay : NSObject

+(void)helperOverlayStart;
+(void)helperOverlayStop;

@end
