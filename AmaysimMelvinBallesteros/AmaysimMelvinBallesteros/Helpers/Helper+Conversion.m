//
//  Helper+Conversion.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Conversion.h"

@implementation Helper_Conversion

+(NSString *)helperConvertCentsToDollar:(double)dollar {
    dollar = dollar/100;
    NSString *formattedDollar = [NSString stringWithFormat:@"$ %0.02f", dollar];
    return formattedDollar;
}


+(NSString *)helperConvertMegaToGiga:(double)value {
    value = value/1000;
    NSString *formattedValue = [NSString stringWithFormat:@"%.02f GB", value];
    return formattedValue;
}

@end
