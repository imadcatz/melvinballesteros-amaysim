//
//  Helper+Json.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Json.h"

@implementation Helper_Json

+(NSDictionary *)helperGetJsonFile {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"collection" ofType:@".json"];
    
    NSError *error;
    NSString *fileContents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    NSData *dataString = [fileContents dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:dataString options:0 error:nil];
    //NSArray *arrayOfResults = [[dicMe objectForKey:@"d"] objectForKey:@"results"];
   
    if (error)
        NSLog(@"Error reading file: %@", error.localizedDescription);
    
    return dictionary;

}

+(NSDictionary *)helperGetAccounts:(NSDictionary *)dictionary {
    NSDictionary *arrayOfAccounts = [dictionary objectForKey:@"data"];
    return arrayOfAccounts;
}


+(NSArray *)helperGetServices:(NSDictionary *)dictionary {
    NSArray *arrayOfAccounts = [dictionary objectForKey:@"included"];
    return arrayOfAccounts;
}


+(NSArray *)helperGetSubscription:(NSDictionary *)dictionary {
    NSArray *arrayOfAccounts = [dictionary objectForKey:@"included"];
    return arrayOfAccounts;
}


+(NSArray *)helperGetProducts:(NSDictionary *)dictionary {
    NSArray *arrayOfAccounts = [[dictionary objectForKey:@"data"] objectForKey:@"included"];
    return arrayOfAccounts;
}

@end
