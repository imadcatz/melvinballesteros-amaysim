//
//  Helper+LayerGradient.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper_LayerGradient : NSObject

+(CAGradientLayer *)helperGradientLinearBackground:(UIColor *)color1 color2:(UIColor *)color2 view:(UIView *)view;

@end
