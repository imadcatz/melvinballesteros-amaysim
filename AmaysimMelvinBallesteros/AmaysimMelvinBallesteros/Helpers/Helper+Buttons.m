//
//  Helper+Buttons.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Buttons.h"

@implementation Helper_Buttons

+(UIButton *)helperCreateButtonLogin:(CGRect)frame text:(NSString *)text {
    UIButton *btnLogin = [[UIButton alloc] initWithFrame:frame];
    [btnLogin setTitle:text forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicBold] size:18]];
    [btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLogin setBackgroundImage:[Helper_Image imageWithColor:UIColorFromRGB(0xE65013)] forState:UIControlStateNormal];
    
    return btnLogin;
}

@end
