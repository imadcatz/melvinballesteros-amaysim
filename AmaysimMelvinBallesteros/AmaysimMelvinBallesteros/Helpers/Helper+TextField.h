//
//  Helper+TextField.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper+Fonts.h"

@interface Helper_TextField : NSObject

+(UITextField *)helperTextFieldLoginEmail:(CGRect)frame;
+(UITextField *)helperTextFieldLoginPassword:(CGRect)frame;

@end
