//
//  Helper+Lines.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper_Lines : NSObject

+(CAShapeLayer *)helperCreateHorizontalLine:(CGPoint)moveToPoint lineToPoint:(CGPoint)lineToPoint;

@end
