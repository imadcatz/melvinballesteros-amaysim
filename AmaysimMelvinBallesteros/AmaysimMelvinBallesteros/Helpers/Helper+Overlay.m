//
//  Helper+Overlay.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Overlay.h"

@implementation Helper_Overlay

+(void)helperOverlayStart {
    if ([AmaysimApplication sharedInstance].overlayView) {
        [[AmaysimApplication sharedInstance].overlayView removeFromSuperview];
    }
    
    [AmaysimApplication sharedInstance].overlayView = [[OverlayView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:[AmaysimApplication sharedInstance].overlayView];
}

+(void)helperOverlayStop {
    [UIView animateWithDuration:0.5 animations:^{
        [[AmaysimApplication sharedInstance].overlayView setAlpha:0];
    } completion:^(BOOL finished) {
        [[AmaysimApplication sharedInstance].overlayView removeFromSuperview];
    }];
}

@end
