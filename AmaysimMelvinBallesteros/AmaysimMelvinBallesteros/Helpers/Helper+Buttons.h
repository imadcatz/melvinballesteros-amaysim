//
//  Helper+Buttons.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper+Image.h"
#import "Helper+Fonts.h"

@interface Helper_Buttons : NSObject

+(UIButton *)helperCreateButtonLogin:(CGRect)frame text:(NSString *)text;

@end
