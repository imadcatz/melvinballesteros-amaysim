//
//  Helper+Fonts.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Fonts.h"

@implementation Helper_Fonts

+(NSString *)fontAppleGothicRegular {
    return @"AppleSDGothicNeo-Regular";
}

+(NSString *)fontAppleGothicBold {
    return @"AppleSDGothicNeo-Bold";
}

+(NSString *)fontAppleGothicMedium {
    return @"AppleSDGothicNeo-Medium";
}

+(NSString *)fontAppleGothicLight {
    return @"AppleSDGothicNeo-Light";
}

@end
