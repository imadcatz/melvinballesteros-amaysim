//
//  Helper+Lines.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Lines.h"

@implementation Helper_Lines

+(CAShapeLayer *)helperCreateHorizontalLine:(CGPoint)moveToPoint lineToPoint:(CGPoint)lineToPoint {
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:moveToPoint];
    [path addLineToPoint:lineToPoint];

    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = UIColorFromRGB(0xE65013).CGColor;
    shapeLayer.lineWidth = 0.8;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    
    return shapeLayer;
}

@end
