//
//  Helper+Labels.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+Labels.h"

@implementation Helper_Labels

+(UILabel *)helperLabelLoginHeader:(CGRect)frame text:(NSString *)text {
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:frame];
    [lblHeader setText:text];
    [lblHeader setBackgroundColor:[UIColor clearColor]];
    [lblHeader setTextColor:UIColorFromRGB(0xE65013)];
    [lblHeader setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicMedium] size:13]];
    [lblHeader sizeToFit];
    
    return lblHeader;
}

+(UILabel *)helperLabelTitleHeader:(CGRect)frame text:(NSString *)text {
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:frame];
    [lblHeader setText:text];
    [lblHeader setBackgroundColor:[UIColor clearColor]];
    [lblHeader setTextColor:UIColorFromRGB(0xF3C6B4)];
    [lblHeader setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicBold] size:23]];
    [lblHeader sizeToFit];
    [lblHeader setFrame:CGRectMake((screenWidth-lblHeader.frame.size.width)/2, lblHeader.frame.origin.y, lblHeader.frame.size.width, lblHeader.frame.size.height)];
    [lblHeader setFrame:CGRectIntegral(lblHeader.frame)];
    
    return lblHeader;
}

+(UILabel *)helperLabelHeader:(CGRect)frame text:(NSString *)text {
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:frame];
    [lblHeader setBackgroundColor:[UIColor clearColor]];
    [lblHeader setText:text];
    [lblHeader setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicRegular] size:11]];
    [lblHeader setTextColor:[UIColor whiteColor]];
    [lblHeader sizeToFit];
    
    return lblHeader;
}



+(UILabel *)helperLabelHeaderValues:(CGRect)frame text:(NSString *)text {
    UILabel *lblHeaderValues = [[UILabel alloc] initWithFrame:frame];
    [lblHeaderValues setBackgroundColor:[UIColor clearColor]];
    [lblHeaderValues setText:text];
    [lblHeaderValues setFont:[UIFont fontWithName:[Helper_Fonts fontAppleGothicMedium] size:18]];
    [lblHeaderValues setTextColor:[UIColor whiteColor]];
    [lblHeaderValues sizeToFit];

    return lblHeaderValues;
}

@end
