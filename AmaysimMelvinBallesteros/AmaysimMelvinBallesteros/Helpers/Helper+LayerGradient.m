//
//  Helper+LayerGradient.m
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import "Helper+LayerGradient.h"

@implementation Helper_LayerGradient

+(CAGradientLayer *)helperGradientLinearBackground:(UIColor *)color1 color2:(UIColor *)color2 view:(UIView *)view {
    CAGradientLayer *layerGradient = [CAGradientLayer layer];
    layerGradient.colors = [NSArray arrayWithObjects: (id)color1.CGColor, (id)color2.CGColor,nil];
    layerGradient.frame = view.bounds;
    
    //start point
    layerGradient.startPoint = CGPointMake(0.0, 0.0);
    layerGradient.endPoint = CGPointMake(0, 1.5);
    return layerGradient;
}

@end
