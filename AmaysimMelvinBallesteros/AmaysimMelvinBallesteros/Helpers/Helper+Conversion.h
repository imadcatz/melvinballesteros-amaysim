//
//  Helper+Conversion.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 29/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper_Conversion : NSObject

+(NSString *)helperConvertCentsToDollar:(double)dollar;
+(NSString *)helperConvertMegaToGiga:(double)value;

@end
