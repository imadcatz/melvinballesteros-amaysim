//
//  Helper+Labels.h
//  AmaysimMelvinBallesteros
//
//  Created by Melvin Ballesteros on 28/05/2017.
//  Copyright © 2017 imadcatz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper+Fonts.h"

@interface Helper_Labels : NSObject

+(UILabel *)helperLabelLoginHeader:(CGRect)frame text:(NSString *)text;
+(UILabel *)helperLabelTitleHeader:(CGRect)frame text:(NSString *)text;

+(UILabel *)helperLabelHeader:(CGRect)frame text:(NSString *)text;
+(UILabel *)helperLabelHeaderValues:(CGRect)frame text:(NSString *)text;

@end
